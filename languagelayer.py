# NOTE: We know, that this API has its Problems with short texts, but this program is just a sample
import requests

# personal api key
apiKey = 'bcf51af100ba73290fc33ecf77cbcbba'

# welcome
print('Hey, wir helfen dir die richtige Sprache herauszufinden :-)')

# this is the query, which will be evaluated
text = input('Type the string you want to look for...')

# http request
response = requests.get('http://api.languagelayer.com/detect?access_key=' + apiKey + '&query=' + text)

# accessing first result (highest probability)
top_result = response.json()['results'][0]
# extract information
probability = top_result['probability']
language = top_result['language_name']

# giving back the result with custimized text
if probability > 50:
    answer = "Wir sind uns sicher dein Text ist in %s geschrieben." %language
elif probability > 10:
    answer = "Wir können es nicht genau sagen, aber es könnte gut %s sein" %language
else:
    answer = "Wir würden lieber gar keinen Tipp abgeben, aber wenn es sein muss... %s ?" %language

print(answer)
